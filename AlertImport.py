import sqlite3
import sys


class AlertImport:

    @staticmethod
    def message_import(index):
        """returns email 1 index higher recent than supplied index"""

        import_sql = "SELECT contents FROM emails WHERE id == {}".format(index + 1)
        try:
            with sqlite3.connect('email_import/email_db.sqlite3') as conn:
                cur = conn.cursor()
                cur.execute(import_sql)
                resp = cur.fetchall()

                if len(resp) == 0:
                    return None
                else:
                    resp = resp[0][0]
                    return resp
        except FileNotFoundError:
            AlertImport.no_alerts()
        except Exception as e:
            raise e

    @staticmethod
    def index_fetch():
        """Loads the last known index from the index file, calls index_init() if none is found"""
        # Open index file, create new index file if none found
        try:
            with open("bot_index.txt") as indextxt:
                index = indextxt.read()
        except FileNotFoundError:
            print("Index file not found. Creating new file...")
            index = AlertImport.index_init()
            return index

        # Ensure index is a numerical
        try:
            index = int(index)
            return index
        except ValueError:
            print("Index file corrupt. Creating new index...")
            index = int(AlertImport.index_init())
            return index

    @staticmethod
    def index_init():
        """Initializes index file and sets index to the highest index in the alerts.csv file"""

        # Fetch latest index from alerts.csv
        try:
            with sqlite3.connect('email_import/email_db.sqlite3') as conn:
                index_sql = "SELECT MAX(id) FROM emails"
                cur = conn.cursor()
                cur.execute(index_sql)
                resp = cur.fetchall()

                latest_index = resp[0][0]
        except sqlite3.OperationalError:
            print("index_init could not connect to SQLITE3 Database")
            sys.exit(1)
        except Exception as e:
            print("UNDEFINED EXCEPTION in index_init:", e)

        # Initialize index file
        try:
            with open('bot_index.txt', 'w') as indextxt:
                indextxt.write(str(latest_index))
        except Exception as e:
            print("Error in index_init:", e)

        # Pass new index back
        print("New index successfully created.")
        return latest_index

    @staticmethod
    def index_update(index):
        """updates the index in the index file"""
        try:
            with open("bot_index.txt", "w") as indextxt:
                indextxt.write(str(index))

        except Exception as e:
            print("Error in index_update:", e)

    @staticmethod
    def no_alerts():
        """Prints an error when alerts.csv isn't found"""
        raise FileNotFoundError("ERROR: No sqlite database found. \
        Please ensure email_db.sqlite3 is present in email_import directory")
