import datetime
import email
import imaplib
import os
import sqlite3
import time

from creds import imap_server
from creds import password
from creds import username


def main():
    print("Email importer importing from: \n"
          "account: {}\n"
          "on server: {} \n"
          "------".format(username, imap_server))
    while True:

        if not os.path.isfile('email_db.sqlite3'):
            sql_init()
            latest_id = 1
        else:
            latest_id = get_id()
            latest_id += 1

        latest_mail = importer()

        current_date = datetime.datetime.now().strftime("%Y-%m-%d")
        sql_write(latest_id, latest_mail, current_date)
        print("email logged at {}".format(current_date))


def importer():
    """Imports UW alert emails from IMAP server"""
    while True:
        time.sleep(60)

        with imaplib.IMAP4_SSL(imap_server) as mail:
            mail.login(username, password)
            # select unread emails
            mail.select("inbox")
            result, data = mail.uid('search', None, "UNSEEN")
            inbox_item_list = data[0].split()

            # if there are unread emails, parse them
            if len(inbox_item_list) > 0:

                # decode formatting
                for item in inbox_item_list:
                    result2, email_data = mail.uid('fetch', item, '(RFC822)')
                    raw_email = email_data[0][1].decode("utf-8")
                    email_message = email.message_from_string(raw_email)

                    # find emails
                    counter = 1
                    for part in email_message.walk():
                        if part.get_content_maintype() == "multipart":
                            continue
                        filename = part.get_filename()
                        if not filename:
                            ext = '.html'
                            filename = 'msg-part-%08d%s' % (counter, ext)
                        counter += 1
                        content_type = part.get_content_type()

                        # check for uw alert
                        payload = part.get_payload()
                        if "plain" in content_type and "alert.uw.edu" in payload:
                            return payload

                # if no unread emails, continue loop
                else:
                    continue


def sql_write(mail_id, mail_contents, date):
    """Writes fetched email to SQLite database"""

    email_sql = "INSERT INTO emails (id, contents, date_pulled) VALUES (?, ?, ?)"

    with sqlite3.connect('email_db.sqlite3') as conn:
        cur = conn.cursor()

        cur.execute(email_sql, (mail_id, mail_contents, date))
        conn.commit()


def sql_init():
    """Initializes SQLite database"""

    print("No SQLite database found. Initializing new database...")

    email_init_sql = """
    CREATE TABLE emails(
    id INT PRIMARY KEY NOT NULL,
    contents TEXT NOT NULL,
    date_pulled TEXT NOT NULL
    );
    """

    with sqlite3.connect('email_db.sqlite3') as conn:
        cur = conn.cursor()
        cur.execute(email_init_sql)

    print("SQLite database initialized.")


def get_id():
    """Retrieves most recent id from SQLite database"""

    with sqlite3.connect('email_db.sqlite3') as conn:
        cur = conn.cursor()

        max_id_sql = "SELECT MAX(id) FROM emails"
        cur.execute(max_id_sql)
        max_id = cur.fetchall()

        if max_id == [(None,)]:
            max_id = 0
            return max_id
        return max_id[0][0]


if __name__ == '__main__':
    main()
