import asyncio

import discord

from AlertImport import AlertImport as ai
from setup import bossid
from setup import botkey
from setup import channelid

client = discord.Client()


async def uw_alert():
    """fetches UW alerts and posts them in the channel"""

    global boss
    global mail_fetch_error_occurred

    await client.wait_until_ready()
    channel = discord.Object(id=channelid)

    # Sets the user to whom error messages are sent
    boss = await client.get_user_info(user_id=bossid)

    # Sets default value for whether a mail fetching error has occurred
    mail_fetch_error_occurred = False

    while not client.is_closed:
        await asyncio.sleep(10)  # task runs every 10 seconds
        index = ai.index_fetch()

        if not mail_fetch_error_occurred:
            try:
                alert = ai.message_import(index)

                if alert is not None:
                    await client.send_message(channel, alert)
                    index += 1
                    ai.index_update(index)
                else:
                    continue
            except Exception as e:
                await client.send_message(boss, "An error occurred during ai.message(import): {} \n"
                                                "reply with 'reset error' to reset me".format(e))
                mail_fetch_error_occurred = True
                continue


@client.event
async def on_ready():
    """prints login confirmation"""

    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


@client.event
async def on_message(message):
    """Allows 'boss' to reset after errors"""

    global mail_fetch_error_occurred

    # ignores messages sent by bot
    if message.author == client.user:
        return

    if 'reset error' in message.content.lower() and message.author == boss:
        if mail_fetch_error_occurred:
            mail_fetch_error_occurred = False
            await client.send_message(boss, "Got it, I'll try again.")
        elif not mail_fetch_error_occurred:
            await client.send_message(boss, "There's currently no error inhibiting me.")

client.loop.create_task(uw_alert())
client.run(botkey)
